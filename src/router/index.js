import Vue from 'vue'
import Router from 'vue-router'
import ProductList from '@/components/ProductList'
import SubmitReview from '@/components/SubmitReview'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'ProductList',
      component: ProductList
    },
    {
      path: '/submit-reviews',
      name: 'SubmitReviews',
      component: SubmitReview
    },
    {
      path: '*',
      component: ProductList
    }
  ]
})
